// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-Arkmanager-Integration <https://gitlab.com/mze9412-discord-bot-project/bot-arkmanager-integration>.
//
// Bot-Arkmanager-Integration is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-Arkmanager-Integration is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-Arkmanager-Integration. If not, see <http://www.gnu.org/licenses/>.

import { DatabaseProviderBase } from "@mze9412-discord-bot-project/bot-database-interface";
import { BotLogger, ConfigService, TYPES_BOTRUNNER } from "@mze9412-discord-bot-project/bot-runner";
import { inject, injectable } from "inversify";
import { ArkManagerIntegrationConfig } from "../models/arkmanagerIntegrationConfig";

@injectable()
export class ArkManagerIntegrationConfigDatabaseProvider extends DatabaseProviderBase<ArkManagerIntegrationConfig> {
    constructor(
        @inject(TYPES_BOTRUNNER.ConfigService) configService: ConfigService,
        @inject(TYPES_BOTRUNNER.BotLogger) botLogger: BotLogger
    ) {
        super(configService, 'ArkManagerIntegrationConfig', 'ArkManagerIntegrationConfig', botLogger);
    }

    protected async storeCore(data: ArkManagerIntegrationConfig, overwriteIfExists: boolean): Promise<boolean> {
        const exists = await this.exists(data);

        if (exists && overwriteIfExists) {
            await this.deleteCore(data);
        } else if (exists) {
            return false;
        }

        const client = await this.connect();
        const collection = await this.openCollection(client);

        const insertResult = await collection.insertOne(data);
        
        await client.close();        
        return insertResult.acknowledged;
    }

    protected async existsCore(data: ArkManagerIntegrationConfig): Promise<boolean> {
        return await this.get(data.guildId) != null;        
    }

    public async get(guildId: string): Promise<ArkManagerIntegrationConfig | null> {
        const release = await this.mutex.acquire();
        try {
            const client = await this.connect();
            const collection = await this.openCollection(client);

            const result = await collection.findOne({ guildId: guildId });
            
            await client.close();
            return result;
        } finally {
            release();
        }
    }

    public async getAll(): Promise<ArkManagerIntegrationConfig[]> {
        const release = await this.mutex.acquire();
        try {
            const client = await this.connect();
            const collection = await this.openCollection(client);

            const result = await collection.find({}).toArray();
            
            await client.close();
            return result;
        } finally {
            release();
        }
    }
    
    protected async deleteCore(data: ArkManagerIntegrationConfig): Promise<number> {
        const client = await this.connect();
        const collection = await this.openCollection(client);

        const deleteResult = await collection.deleteMany({guildId: data.guildId});

        await client.close();
        return deleteResult.deletedCount == undefined ? 0 : <number>deleteResult.deletedCount;
    }

    protected async deleteAllCore(guildId: string): Promise<number> {
        const client = await this.connect();
        const collection = await this.openCollection(client);

        const deleteResult = await collection.deleteMany({guildId: guildId});

        await client.close();
        return deleteResult.deletedCount == undefined ? 0 : <number>deleteResult.deletedCount;
    }
    
    protected async createIndexCore(): Promise<void> {
        const client = await this.connect();
        const collection = await this.openCollection(client);

        await collection.createIndex({ guildId: 1 });
        await client.close();
    }
}