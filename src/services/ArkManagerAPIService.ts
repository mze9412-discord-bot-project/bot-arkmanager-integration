// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-Arkmanager-Integration <https://gitlab.com/mze9412-discord-bot-project/bot-arkmanager-integration>.
//
// Bot-Arkmanager-Integration is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-Arkmanager-Integration is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-Arkmanager-Integration. If not, see <http://www.gnu.org/licenses/>.

import { ArkServer, ArkServerDatabaseProvider, ServerQueryService, TYPES_ARKMANAGEMENT } from "@mze9412-discord-bot-project/bot-ark-management";
import { BotLogger, DiscordChannelService, TYPES_BOTRUNNER } from "@mze9412-discord-bot-project/bot-runner";
import { inject, injectable } from "inversify";
import { ArkManagerIntegrationConfigDatabaseProvider } from "../database/arkmanagerIntegrationConfigDatabaseProvider";
import { TYPES_ARKMANAGERINTEGRATION } from "../TYPES";
import axios from 'axios';
import https from 'https';
import { Logger } from "tslog";
import { Client } from "discord.js";

@injectable()
export class ArkManagerAPIService {
    private _logger!: Logger;

    // last time a command was called, i.e. update, start, etc.
    private _lastCommandDate = new Date();

    /**
     * CTOR
     */
    constructor(
        @inject(TYPES_ARKMANAGERINTEGRATION.ArkManagerIntegrationConfigDatabaseProvider) private _arkManagerIntegrationProvider: ArkManagerIntegrationConfigDatabaseProvider,
        @inject(TYPES_ARKMANAGEMENT.ArkServerDatabaseProvider) private _arkServerProvider: ArkServerDatabaseProvider,
        @inject(TYPES_BOTRUNNER.DiscordChannelService) private _discordChannelService: DiscordChannelService,
        @inject(TYPES_ARKMANAGEMENT.ServerQueryService) private _serverQueryService: ServerQueryService,
        @inject(TYPES_BOTRUNNER.BotLogger) private _botLogger: BotLogger
    ) {
        this._logger = _botLogger.getChildLogger('ArkManagerAPIService');
    }

    public async restartAll(guildId: string): Promise<boolean> {
        // get all servers
        const servers = await this._arkServerProvider.getAll(guildId);
        if (servers.length === 0) {
            return false;
        }

        // get config
        const config =  await this._arkManagerIntegrationProvider.get(guildId);
        if (config == undefined) {
            return false;
        }

        // get channel
        const channel = await this._discordChannelService.getTextChannelByName(guildId, config.infoChannel);
        if (channel == undefined) {
            return false;
        }

        // set last command date
        this._lastCommandDate = new Date();

        // define async method
        const restartMethod = async () => {
            await channel.send(`Restart of all servers running. All servers should be available in about 10 minutes..`);
            for (const server of servers) {
                const url = `https://${server.internalIp}:${config.apiPort}/command/${config.apiToken}`;
                const payload = {
                    serverName: config.useDefaultInstance ? '' : server.name,
                    command: "restart",
                };
                try {
                    await axios.post(url, payload, {httpsAgent: new https.Agent({ rejectUnauthorized: false })});
                } catch(ex) { this._logger.error(ex); }
            }
        }

        // check if players are online
        const arePlayersOnline = await this.arePlayersOnline(servers);
        if (!arePlayersOnline) {
            setTimeout(restartMethod, 5000);
        } else {
            // start warnings and trigger restart
            setTimeout(async () => await channel.send(`Restart of all servers starts in 10 minutes.`), 0*1000);
            setTimeout(async () => await channel.send(`Restart of all servers starts in 9 minutes.`), 60*1000);
            setTimeout(async () => await channel.send(`Restart of all servers starts in 8 minutes.`), 120*1000);
            setTimeout(async () => await channel.send(`Restart of all servers starts in 7 minutes.`), 180*1000);
            setTimeout(async () => await channel.send(`Restart of all servers starts in 6 minutes.`), 240*1000);
            setTimeout(async () => await channel.send(`Restart of all servers starts in 5 minutes.`), 300*1000);
            setTimeout(async () => await channel.send(`Restart of all servers starts in 4 minutes.`), 360*1000);
            setTimeout(async () => await channel.send(`Restart of all servers starts in 3 minutes.`), 420*1000);
            setTimeout(async () => await channel.send(`Restart of all servers starts in 2 minutes.`), 480*1000);
            setTimeout(async () => await channel.send(`Restart of all servers starts in 1 minutes.`), 540*1000);
            setTimeout(async () => await channel.send(`Restart of all servers starts in 30 seconds.`), 570*1000);
            setTimeout(async () => await channel.send(`Restart of all servers starts in 20 seconds.`), 580*1000);
            setTimeout(async () => await channel.send(`Restart of all servers starts in 10 seconds.`), 590*1000);
            setTimeout(restartMethod, 600*1000);
        }

        return true;
    }

    public async restart(guildId: string, serverName: string): Promise<boolean> {
        // get server
        const server = await this._arkServerProvider.get(guildId, serverName);
        if (server == undefined) {
            this._logger.error('Cannot find server.');
            return false;
        }

        // get config
        const config =  await this._arkManagerIntegrationProvider.get(guildId);
        if (config == undefined) {
            this._logger.error('ArkManagement module not configured.');
            return false;
        }

        // get channel
        const channel = await this._discordChannelService.getTextChannelByName(guildId, config.infoChannel);
        if (channel == undefined) {
            this._logger.error('No channel defined for restart info logging.');
            return false;
        }

        // set last command date
        this._lastCommandDate = new Date();

        // define restart method
        const restartMethod = async () => {
            await channel.send(`Restart of ${server.friendlyName} running. The server should be available in about 10 minutes..`);
            const url = `https://${server.internalIp}:${config.apiPort}/command/${config.apiToken}`;
            const payload = {
                serverName: config.useDefaultInstance ? '' : server.name,
                command: "restart",
            };
            try {
                await axios.post(url, payload, {httpsAgent: new https.Agent({ rejectUnauthorized: false })});
            } catch(ex) { this._logger.error(ex); }
        }

        // check if players are online
        const arePlayersOnline = await this.arePlayersOnline([server]);
        if (!arePlayersOnline) {
            setTimeout(restartMethod, 5000);
        } else {
            // start warnings and trigger restart
            setTimeout(async () => await channel.send(`Restart of ${server.friendlyName} starts in 10 minutes.`), 0*1000);
            setTimeout(async () => await channel.send(`Restart of ${server.friendlyName} starts in 9 minutes.`), 60*1000);
            setTimeout(async () => await channel.send(`Restart of ${server.friendlyName} starts in 8 minutes.`), 120*1000);
            setTimeout(async () => await channel.send(`Restart of ${server.friendlyName} starts in 7 minutes.`), 180*1000);
            setTimeout(async () => await channel.send(`Restart of ${server.friendlyName} starts in 6 minutes.`), 240*1000);
            setTimeout(async () => await channel.send(`Restart of ${server.friendlyName} starts in 5 minutes.`), 300*1000);
            setTimeout(async () => await channel.send(`Restart of ${server.friendlyName} starts in 4 minutes.`), 360*1000);
            setTimeout(async () => await channel.send(`Restart of ${server.friendlyName} starts in 3 minutes.`), 420*1000);
            setTimeout(async () => await channel.send(`Restart of ${server.friendlyName} starts in 2 minutes.`), 480*1000);
            setTimeout(async () => await channel.send(`Restart of ${server.friendlyName} starts in 1 minutes.`), 540*1000);
            setTimeout(async () => await channel.send(`Restart of ${server.friendlyName} starts in 30 seconds.`), 570*1000);
            setTimeout(async () => await channel.send(`Restart of ${server.friendlyName} starts in 20 seconds.`), 580*1000);
            setTimeout(async () => await channel.send(`Restart of ${server.friendlyName} starts in 10 seconds.`), 590*1000);
            setTimeout(restartMethod, 600*1000);
        }

        return true;
    }

    public async startAll(guildId: string): Promise<boolean> {
        // get all servers
        const servers = await this._arkServerProvider.getAll(guildId);
        if (servers.length === 0) {
            return false;
        }

        // get config
        const config =  await this._arkManagerIntegrationProvider.get(guildId);
        if (config == undefined) {
            return false;
        }

        // get channel
        const channel = await this._discordChannelService.getTextChannelByName(guildId, config.infoChannel);
        if (channel == undefined) {
            return false;
        }

        // set last command date
        this._lastCommandDate = new Date();

        // start warnings and trigger restart
        await channel.send(`Start of all servers running. All servers should be available in about 10 minutes ...`)
        for (const server of servers) {
            const url = `https://${server.internalIp}:${config.apiPort}/command/${config.apiToken}`;
            const payload = {
                serverName: config.useDefaultInstance ? '' : server.name,
                command: "start",
            };
            try {
                await axios.post(url, payload, {httpsAgent: new https.Agent({ rejectUnauthorized: false })});
            } catch(ex) { this._logger.error(ex); }
        }

        return true;
    }

    public async start(guildId: string, serverName: string): Promise<boolean> {
        // get server
        const server = await this._arkServerProvider.get(guildId, serverName);
        if (server == undefined) {
            return false;
        }

        // get config
        const config =  await this._arkManagerIntegrationProvider.get(guildId);
        if (config == undefined) {
            return false;
        }

        // get channel
        const channel = await this._discordChannelService.getTextChannelByName(guildId, config.infoChannel);
        if (channel == undefined) {
            return false;
        }

        // set last command date
        this._lastCommandDate = new Date();

        // start
        await channel.send(`Start of server ${server.friendlyName} running. The server should be available in about 10 minutes ...`)
        const url = `https://${server.internalIp}:${config.apiPort}/command/${config.apiToken}`;
        const payload = {
            serverName: config.useDefaultInstance ? '' : server.name,
            command: "start",
        };
        try {
            await axios.post(url, payload, {httpsAgent: new https.Agent({ rejectUnauthorized: false })});
        } catch(ex) { this._logger.error(ex); }

        return true;
    }

    public async stopAll(guildId: string): Promise<boolean> {
        // get all servers
        const servers = await this._arkServerProvider.getAll(guildId);
        if (servers.length === 0) {
            return false;
        }

        // get config
        const config =  await this._arkManagerIntegrationProvider.get(guildId);
        if (config == undefined) {
            return false;
        }

        // get channel
        const channel = await this._discordChannelService.getTextChannelByName(guildId, config.infoChannel);
        if (channel == undefined) {
            return false;
        }

        // set last command date
        this._lastCommandDate = new Date();

        // define stop method
        const stopMethod = async () => {
            await channel.send(`Shutdown of all servers running. All servers are going down for maintenance ...`);
            for (const server of servers) {
                const url = `https://${server.internalIp}:${config.apiPort}/command/${config.apiToken}`;
                const payload = {
                    serverName: config.useDefaultInstance ? '' : server.name,
                    command: "stop",
                };
                try {
                    await axios.post(url, payload, {httpsAgent: new https.Agent({ rejectUnauthorized: false })});
                } catch(ex) { this._logger.error(ex); }
            }
        }

        // check if players are online
        const arePlayersOnline = await this.arePlayersOnline(servers);
        if (!arePlayersOnline) {
            setTimeout(stopMethod, 5000);
        } else {
            // start warnings and trigger stop
            setTimeout(async () => await channel.send(`Shutdown of all servers starts in 10 minutes.`), 0*1000);
            setTimeout(async () => await channel.send(`Shutdown of all servers starts in 9 minutes.`), 60*1000);
            setTimeout(async () => await channel.send(`Shutdown of all servers starts in 8 minutes.`), 120*1000);
            setTimeout(async () => await channel.send(`Shutdown of all servers starts in 7 minutes.`), 180*1000);
            setTimeout(async () => await channel.send(`Shutdown of all servers starts in 6 minutes.`), 240*1000);
            setTimeout(async () => await channel.send(`Shutdown of all servers starts in 5 minutes.`), 300*1000);
            setTimeout(async () => await channel.send(`Shutdown of all servers starts in 4 minutes.`), 360*1000);
            setTimeout(async () => await channel.send(`Shutdown of all servers starts in 3 minutes.`), 420*1000);
            setTimeout(async () => await channel.send(`Shutdown of all servers starts in 2 minutes.`), 480*1000);
            setTimeout(async () => await channel.send(`Shutdown of all servers starts in 1 minutes.`), 540*1000);
            setTimeout(async () => await channel.send(`Shutdown of all servers starts in 30 seconds.`), 570*1000);
            setTimeout(async () => await channel.send(`Shutdown of all servers starts in 20 seconds.`), 580*1000);
            setTimeout(async () => await channel.send(`Shutdown of all servers starts in 10 seconds.`), 590*1000);
            setTimeout(stopMethod, 600*1000);
        }

        return true;
    }

    public async stop(guildId: string, serverName: string): Promise<boolean> {
        // get server
        const server = await this._arkServerProvider.get(guildId, serverName);
        if (server == undefined) {
            return false;
        }

        // get config
        const config =  await this._arkManagerIntegrationProvider.get(guildId);
        if (config == undefined) {
            return false;
        }

        // get channel
        const channel = await this._discordChannelService.getTextChannelByName(guildId, config.infoChannel);
        if (channel == undefined) {
            return false;
        }

        // set last command date
        this._lastCommandDate = new Date();

        // define stop method
        const stopMethod = async () => {
            await channel.send(`Shutdown of ${server.friendlyName} running. The server is going down for maintenance ...`);
            const url = `https://${server.internalIp}:${config.apiPort}/command/${config.apiToken}`;
            const payload = {
                serverName: config.useDefaultInstance ? '' : server.name,
                command: "stop",
            };
            try {
                await axios.post(url, payload, {httpsAgent: new https.Agent({ rejectUnauthorized: false })});
            } catch(ex) { this._logger.error(ex); }
        }

        // check if players are online
        const arePlayersOnline = await this.arePlayersOnline([server]);
        if (!arePlayersOnline) {
            setTimeout(stopMethod, 5000);
        } else {
            // start warnings and trigger restart
            setTimeout(async () => await channel.send(`Shutdown of ${server.friendlyName} starts in 10 minutes.`), 0*1000);
            setTimeout(async () => await channel.send(`Shutdown of ${server.friendlyName} starts in 9 minutes.`), 60*1000);
            setTimeout(async () => await channel.send(`Shutdown of ${server.friendlyName} starts in 8 minutes.`), 120*1000);
            setTimeout(async () => await channel.send(`Shutdown of ${server.friendlyName} starts in 7 minutes.`), 180*1000);
            setTimeout(async () => await channel.send(`Shutdown of ${server.friendlyName} starts in 6 minutes.`), 240*1000);
            setTimeout(async () => await channel.send(`Shutdown of ${server.friendlyName} starts in 5 minutes.`), 300*1000);
            setTimeout(async () => await channel.send(`Shutdown of ${server.friendlyName} starts in 4 minutes.`), 360*1000);
            setTimeout(async () => await channel.send(`Shutdown of ${server.friendlyName} starts in 3 minutes.`), 420*1000);
            setTimeout(async () => await channel.send(`Shutdown of ${server.friendlyName} starts in 2 minutes.`), 480*1000);
            setTimeout(async () => await channel.send(`Shutdown of ${server.friendlyName} starts in 1 minutes.`), 540*1000);
            setTimeout(async () => await channel.send(`Shutdown of ${server.friendlyName} starts in 30 seconds.`), 570*1000);
            setTimeout(async () => await channel.send(`Shutdown of ${server.friendlyName} starts in 20 seconds.`), 580*1000);
            setTimeout(async () => await channel.send(`Shutdown of ${server.friendlyName} starts in 10 seconds.`), 590*1000);
            setTimeout(stopMethod, 600*1000);
        }

        return true;
    }

    public async updateAll(guildId: string): Promise<boolean> {
        // get all servers
        const servers = await this._arkServerProvider.getAll(guildId);
        if (servers.length === 0) {
            return false;
        }

        // get config
        const config =  await this._arkManagerIntegrationProvider.get(guildId);
        if (config == undefined) {
            return false;
        }

        // get channel
        const channel = await this._discordChannelService.getTextChannelByName(guildId, config.infoChannel);
        if (channel == undefined) {
            return false;
        }

        // set last command date
        this._lastCommandDate = new Date();

        // define update method
        const updateMethod = async () => {
            await channel.send(`Update of all servers running. All servers should be available in about 10 minutes ...`);
            for (const server of servers) {
                const url = `https://${server.internalIp}:${config.apiPort}/command/${config.apiToken}`;
                const payload = {
                    serverName: config.useDefaultInstance ? '' : server.name,
                    command: "update",
                };
                try {
                    await axios.post(url, payload, {httpsAgent: new https.Agent({ rejectUnauthorized: false })});
                } catch(ex) { this._logger.error(ex); }
            }
        }

        // check if players are online
        const arePlayersOnline = await this.arePlayersOnline(servers);
        if (!arePlayersOnline) {
            setTimeout(updateMethod, 5000);
        } else {
            // start warnings and trigger restart
            setTimeout(async () => await channel.send(`Update of all servers starts in 10 minutes.`), 0*1000);
            setTimeout(async () => await channel.send(`Update of all servers starts in 9 minutes.`), 60*1000);
            setTimeout(async () => await channel.send(`Update of all servers starts in 8 minutes.`), 120*1000);
            setTimeout(async () => await channel.send(`Update of all servers starts in 7 minutes.`), 180*1000);
            setTimeout(async () => await channel.send(`Update of all servers starts in 6 minutes.`), 240*1000);
            setTimeout(async () => await channel.send(`Update of all servers starts in 5 minutes.`), 300*1000);
            setTimeout(async () => await channel.send(`Update of all servers starts in 4 minutes.`), 360*1000);
            setTimeout(async () => await channel.send(`Update of all servers starts in 3 minutes.`), 420*1000);
            setTimeout(async () => await channel.send(`Update of all servers starts in 2 minutes.`), 480*1000);
            setTimeout(async () => await channel.send(`Update of all servers starts in 1 minutes.`), 540*1000);
            setTimeout(async () => await channel.send(`Update of all servers starts in 30 seconds.`), 570*1000);
            setTimeout(async () => await channel.send(`Update of all servers starts in 20 seconds.`), 580*1000);
            setTimeout(async () => await channel.send(`Update of all servers starts in 10 seconds.`), 590*1000);
            setTimeout(updateMethod, 600*1000);
        }
        return true;
    }

    public async update(guildId: string, serverName: string): Promise<boolean> {
        // get server
        const server = await this._arkServerProvider.get(guildId, serverName);
        if (server == undefined) {
            return false;
        }

        // get config
        const config =  await this._arkManagerIntegrationProvider.get(guildId);
        if (config == undefined) {
            return false;
        }

        // get channel
        const channel = await this._discordChannelService.getTextChannelByName(guildId, config.infoChannel);
        if (channel == undefined) {
            return false;
        }

        // set last command date
        this._lastCommandDate = new Date();

        // define update method
        const updateMethod = async () => {
            await channel.send(`Update of ${server.friendlyName} running. The server should be available again in about 10 minutes ...`);
            const url = `https://${server.internalIp}:${config.apiPort}/command/${config.apiToken}`;
            const payload = {
                serverName: config.useDefaultInstance ? '' : server.name,
                command: "update",
            };
            try {
                await axios.post(url, payload, {httpsAgent: new https.Agent({ rejectUnauthorized: false })});
            } catch(ex) { this._logger.error(ex); }
        }

        // check if players are online
        const arePlayersOnline = await this.arePlayersOnline([server]);
        if (!arePlayersOnline) {
            setTimeout(updateMethod, 5000);
        } else {
        // start warnings and trigger restart
        setTimeout(async () => await channel.send(`Update of ${server.friendlyName} starts in 10 minutes.`), 0*1000);
        setTimeout(async () => await channel.send(`Update of ${server.friendlyName} starts in 9 minutes.`), 60*1000);
        setTimeout(async () => await channel.send(`Update of ${server.friendlyName} starts in 8 minutes.`), 120*1000);
        setTimeout(async () => await channel.send(`Update of ${server.friendlyName} starts in 7 minutes.`), 180*1000);
        setTimeout(async () => await channel.send(`Update of ${server.friendlyName} starts in 6 minutes.`), 240*1000);
        setTimeout(async () => await channel.send(`Update of ${server.friendlyName} starts in 5 minutes.`), 300*1000);
        setTimeout(async () => await channel.send(`Update of ${server.friendlyName} starts in 4 minutes.`), 360*1000);
        setTimeout(async () => await channel.send(`Update of ${server.friendlyName} starts in 3 minutes.`), 420*1000);
        setTimeout(async () => await channel.send(`Update of ${server.friendlyName} starts in 2 minutes.`), 480*1000);
        setTimeout(async () => await channel.send(`Update of ${server.friendlyName} starts in 1 minutes.`), 540*1000);
        setTimeout(async () => await channel.send(`Update of ${server.friendlyName} starts in 30 seconds.`), 570*1000);
        setTimeout(async () => await channel.send(`Update of ${server.friendlyName} starts in 20 seconds.`), 580*1000);
        setTimeout(async () => await channel.send(`Update of ${server.friendlyName} starts in 10 seconds.`), 590*1000);
        setTimeout(updateMethod, 600*1000);
        }

        return true;
    }

    public async checkUpdate(guildId: string, serverName: string): Promise<boolean|undefined> {
        // get server
        const server = await this._arkServerProvider.get(guildId, serverName);
        if (server == undefined) {
            this._logger.warn(`[ARKManager] Server ${serverName} not found.`);
            return undefined;
        }

        // get config
        const config =  await this._arkManagerIntegrationProvider.get(guildId);
        if (config == undefined) {
            return undefined;
        }

        // check for update
        const url = `https://${server.internalIp}:${config.apiPort}/command/${config.apiToken}`;
        const payload = {
            serverName: config.useDefaultInstance ? '' : server.name,
            command: 'checkupdate'
        };
        try {
            const res = await axios.post(url, payload, {httpsAgent: new https.Agent({ rejectUnauthorized: false })});
            if (res.status === 200) {
                return res.data.actionRequired;
            }
        } catch(ex) {
            this._logger.error(ex);
        }
    }

    public async checkUpdateAll(guildId: string): Promise<boolean|undefined> {
        // get all servers
        const servers = await this._arkServerProvider.getAll(guildId);
        if (servers.length === 0) {
            return false;
        }

        // get config
        const config =  await this._arkManagerIntegrationProvider.get(guildId);
        if (config == undefined) {
            return undefined;
        }

        // check for update
        let updateRequired = false;
        for (const server of servers) {
            const url = `https://${server.internalIp}:${config.apiPort}/command/${config.apiToken}`;
            const payload = {
                serverName: config.useDefaultInstance ? '' : server.name,
                command: 'checkupdate'
            };
            
            try {
                const res = await axios.post(url, payload, {httpsAgent: new https.Agent({ rejectUnauthorized: false })});
                if (res.status === 200) {
                    updateRequired ||= <boolean>res.data.actionRequired;
                    if (updateRequired) break;
                }
            } catch(ex) {
                this._logger.error(ex);
            }
        }
        return updateRequired;
    }

    private async arePlayersOnline(servers: ArkServer[]): Promise<boolean> {
        let countOnline = 0;

        for(const server of servers) {
            const result = await this._serverQueryService.queryServerDirect(server);
            countOnline += result.playerCount;
        }

        return countOnline > 0;
    }

    public async runAutoUpdateCheck(client: Client<boolean>): Promise<void> {
        if (await this.isLastCommandOlderThan(30)) {
            for(const guild of client.guilds.cache.values()) {
                await this.runAutoUpdateCheckForGuild(guild.id);
            }
        }
    }

    private async runAutoUpdateCheckForGuild(guildId: string): Promise<void> {
        // abort if voting is not active
        this._logger.info(`ArkManager: Automatic Update Check running for guild ${guildId}.`);
        
        // get config
        const config = await this._arkManagerIntegrationProvider.get(guildId);
        if (config == null || !config.useAutomatedUpdateCheck) return;
                
        // run update check
        const updateRequired = await this.checkUpdateAll(guildId);
        this._logger.debug(`ArkManager: Automatic Update Check: Update required for Game or Mod: ${updateRequired}.`);

        if (updateRequired) {
            const channel = await this._discordChannelService.getTextChannelByName(guildId, config.infoChannel);
            if (channel == undefined) {
                this._logger.warn(`ArkManager: Automatic Update Check. Infochannel ${config.infoChannel} does not exist. Create it manually, please!`);
                return;
            }
            
            const highestRoleId = channel.guild.roles.highest.id;
            channel.send({ content: `<@&${highestRoleId}> There is a server or mod update available!` });
        }
    }

    public async isLastCommandOlderThan(minutes: number): Promise<boolean> {
        const diffMs = minutes * 60000; // 60 seconds per minute, 1000 milliseconds per second

        const diff = Date.now() - this._lastCommandDate.getTime();
        return diff > diffMs;
    }
}
