// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-Arkmanager-Integration <https://gitlab.com/mze9412-discord-bot-project/bot-arkmanager-integration>.
//
// Bot-Arkmanager-Integration is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-Arkmanager-Integration is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-Arkmanager-Integration. If not, see <http://www.gnu.org/licenses/>.

import { SlashCommandBuilder } from "@discordjs/builders";
import { BotLogger, BotModule, TYPES_BOTRUNNER } from "@mze9412-discord-bot-project/bot-runner";
import { Mutex } from "async-mutex";
import { BaseCommandInteraction, CacheType, CommandInteractionOption, CommandInteractionOptionResolver } from "discord.js";
import { inject, injectable } from "inversify";
import { schedule, ScheduledTask } from "node-cron";
import { ArkManagerIntegrationConfig } from ".";
import { arkmanagerCheckUpdateCommand } from "./commands/arkManagerCheckUpdateCommand";
import { arkmanagerRestartCommand } from "./commands/arkmanagerRestartCommand";
import { arkmanagerStartCommand } from "./commands/arkmanagerStartCommand";
import { arkmanagerStopCommand } from "./commands/arkmanagerStopCommand";
import { arkmanagerUpdateCommand } from "./commands/arkmanagerUpdateCommand";
import { ArkManagerIntegrationConfigDatabaseProvider } from "./database/arkmanagerIntegrationConfigDatabaseProvider";
import { ArkManagerAPIService } from "./services/ArkManagerAPIService";
import { TYPES_ARKMANAGERINTEGRATION } from "./TYPES";

@injectable()
export class ArkManagerIntegrationBotModule extends BotModule {

    private _autoUpdateCheckTask: ScheduledTask | undefined = undefined;
    private _updateMutex = new Mutex();
    
    constructor(
        @inject(TYPES_ARKMANAGERINTEGRATION.ArkManagerAPIService) private _apiService: ArkManagerAPIService,
        @inject(TYPES_ARKMANAGERINTEGRATION.ArkManagerIntegrationConfigDatabaseProvider) private _arkManagerIntegrationConfigProvider: ArkManagerIntegrationConfigDatabaseProvider,
        @inject(TYPES_BOTRUNNER.BotLogger) _botLogger: BotLogger
    ) {
        super('arkmanager', 'Manage your servers via arkmanager remote integration. Requires arkmanager-integration-api.', _botLogger)
    }

    async initializeCore(): Promise<void> {
        this.addSubCommand(new arkmanagerCheckUpdateCommand(this._apiService));
        this.addSubCommand(new arkmanagerUpdateCommand(this._apiService));
        this.addSubCommand(new arkmanagerStartCommand(this._apiService));
        this.addSubCommand(new arkmanagerStopCommand(this._apiService));
        this.addSubCommand(new arkmanagerRestartCommand(this._apiService));
    }

    async buildConfigCommand(configCommandBuilder: SlashCommandBuilder): Promise<void> {
        configCommandBuilder.addSubcommand(scmd =>
            scmd.setName(this.name)
            .setDescription('Module specific configuration')

            .addStringOption(opt =>
                opt.setName('apitoken')
                .setDescription('What is your ArkManager API token?')
                .setRequired(true)
            )
            .addIntegerOption(opt =>
                opt.setName('port')
                .setDescription('What is your ArkManager API port?')
                .setRequired(true)
            )
            .addStringOption(opt =>
                opt.setName('infochannel')
                .setDescription('What channel should I post warnings/information to?')
                .setRequired(true)
            )
            .addBooleanOption(opt =>
                opt.setName('usedefaultinstance')
                .setDescription('Depending on your setup, should I use default instances or named @instances?')
                .setRequired(true)
            )
            .addBooleanOption(opt =>
                opt.setName('useautomatedupdatecheck')
                .setDescription('Do you want automatic update availability checks every 15 minutes?')
                .setRequired(true)
            )
        );
    }

    async handleConfigCommand(interaction: BaseCommandInteraction<CacheType>): Promise<void> {
        if (interaction.guild == null) return;

        const opts = <CommandInteractionOptionResolver>interaction.options;
        const subCommand = <CommandInteractionOption[]>opts.data;
        const options = <CommandInteractionOption[]>subCommand[0].options;
        
        const vals = new ArkManagerIntegrationConfig();
        vals.guildId = interaction.guild.id;
        
        for(const o of options) {
            if (o.name === 'apitoken') vals.apiToken = <string>o.value;
            if (o.name === 'port') vals.apiPort = <number>o.value;
            if (o.name === 'infochannel') vals.infoChannel = <string>o.value;
            if (o.name === 'usedefaultinstance') vals.useDefaultInstance = <boolean>o.value;
            if (o.name === 'useautomatedupdatecheck') vals.useAutomatedUpdateCheck = <boolean>o.value;

            this.logger.debug(`Configured option: ${o.name}. Value: ${o.value}`);
        }
        
        const result = await this._arkManagerIntegrationConfigProvider.store(vals, true);
        this.logger.debug('Saving ArkServerManagementConfig finished. Success: ' + result);
        await interaction.reply(`Configuration for ${this.name} saved!`);
    }
    
    public async start(): Promise<void> {
        this.logger.info('ArkManagerIntegrationBotModule: Start');

        // if (this._autoUpdateCheckTask == undefined) {
        //     this.logger.info('Scheduling Automatic Update Check Task.');
        //     this._autoUpdateCheckTask = schedule('*/30 * * * *', () => {
        //         this.runAutoUpdateCheck().then()
        //     });
        //     this._autoUpdateCheckTask.start();
        // }
    }
    
    async stop(): Promise<void> {
        this.logger.info('ArkVotingBotModule: Stop');
        if (this._autoUpdateCheckTask != undefined) {
            this._autoUpdateCheckTask.stop();
            this._autoUpdateCheckTask = undefined;
        }
    }

    private async runAutoUpdateCheck(): Promise<void> {
        this.logger.info('Running Automatic Update Check Update.');
        const release = await this._updateMutex.acquire();
        try {
            await this._apiService.runAutoUpdateCheck(this.client);
        } finally {
            release();
            this.logger.info('Finished Automatic Update Check.');
        }
    }
}