// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-Arkmanager-Integration <https://gitlab.com/mze9412-discord-bot-project/bot-arkmanager-integration>.
//
// Bot-Arkmanager-Integration is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-Arkmanager-Integration is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-Arkmanager-Integration. If not, see <http://www.gnu.org/licenses/>.

import { SlashCommandSubcommandBuilder, SlashCommandSubcommandGroupBuilder, SlashCommandBuilder } from "@discordjs/builders";
import { BotCommand } from "@mze9412-discord-bot-project/bot-runner";
import { BaseCommandInteraction, CommandInteractionOption } from "discord.js";
import { ArkManagerAPIService } from "../services/ArkManagerAPIService";

export class arkmanagerRestartCommand extends BotCommand {
    constructor(private _apiService: ArkManagerAPIService) {
        super('restart', 'Restart a server.', 'ADMINISTRATOR');
    }

    buildCommand(): SlashCommandSubcommandBuilder | SlashCommandSubcommandGroupBuilder | SlashCommandBuilder {
        const subCmdBuilder = new SlashCommandSubcommandBuilder()
            .setName(this.name)
            .setDescription(this.description)
            .addStringOption(o =>
                o.setName('servername')
                    .setDescription('Name of the server to restart. Leave empty for all.')
            );
            

        return subCmdBuilder;
    }

    async isMatching(interaction: BaseCommandInteraction, moduleName: string): Promise<boolean> {
        const opts = interaction.options;
        const commandName = interaction.commandName;
        const subCommandName = opts.data.length > 0 ? opts.data[0].name : '';
        return commandName === moduleName && subCommandName === this.name;
    }

    async execute(interaction: BaseCommandInteraction): Promise<void> {
        if (interaction.guild == null) return;
        
        if (!interaction.memberPermissions?.has("ADMINISTRATOR")) {
            await interaction.reply({ content: 'Sorry, you are not allowed to use this command.', ephemeral: true });
            return;
        }

        const opts = interaction.options;
        const subCommand = <CommandInteractionOption>opts.data[0];
        const parameters = <CommandInteractionOption[]>subCommand.options;

        let servername = '';
        if (parameters != undefined) {
            for (const param of parameters) {
                if (param.name === 'servername') servername = <string>param.value;
            }
        }

        await interaction.reply({ content: 'Executing restart. Please stand by ...', ephemeral: true });
        let result = false;
        if (servername === '') {
            result = await this._apiService.restartAll(interaction.guild.id);
        } else {
            result = await this._apiService.restart(interaction.guild.id, servername);
        }
        
        if (!result) {
            await interaction.followUp('Failed to trigger restart. Did you configure the integration properly and define an existing channel for feedback? Also check if you did add any servers.');
        } else {
            await interaction.followUp(`Initiated server restart for ${servername === '' ? 'all servers' : servername}.`);
        }        
    }
}