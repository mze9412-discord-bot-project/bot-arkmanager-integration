// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-Arkmanager-Integration <https://gitlab.com/mze9412-discord-bot-project/bot-arkmanager-integration>.
//
// Bot-Arkmanager-Integration is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-Arkmanager-Integration is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-Arkmanager-Integration. If not, see <http://www.gnu.org/licenses/>.

import { Container } from "inversify";
import { BotModule, BotRunner, ConfigService, DefaultModule, DiscordChannelService, DiscordMessageService, DiscordUserService, TYPES_BOTRUNNER } from "@mze9412-discord-bot-project/bot-runner";
import { DiscordSlugDatabaseProvider, DiscordSlugService, TYPES_BOTSHARED } from "@mze9412-discord-bot-project/bot-shared";
import { ArkManagerIntegrationBotModule, ArkManagerIntegrationConfigDatabaseProvider } from "..";
import { ArkManagementBotModule, ArkServerDatabaseProvider, ArkServerManagementConfigDatabaseProvider, DataUpdateService, RconService, ServerQueryService, TYPES_ARKMANAGEMENT } from "@mze9412-discord-bot-project/bot-ark-management";
import { TYPES_ARKMANAGERINTEGRATION } from "../TYPES";
import { ArkManagerAPIService } from "../services/ArkManagerAPIService";

const myContainer = new Container();
myContainer.bind<ConfigService>(TYPES_BOTRUNNER.ConfigService).to(ConfigService).inSingletonScope();
myContainer.bind<BotRunner>(TYPES_BOTRUNNER.BotRunner).to(BotRunner).inSingletonScope();

myContainer.bind<DiscordChannelService>(TYPES_BOTRUNNER.DiscordChannelService).to(DiscordChannelService).inSingletonScope();
myContainer.bind<DiscordMessageService>(TYPES_BOTRUNNER.DiscordMessageService).to(DiscordMessageService).inSingletonScope();
myContainer.bind<DiscordUserService>(TYPES_BOTRUNNER.DiscordUserService).to(DiscordUserService).inSingletonScope();
myContainer.bind<DiscordSlugDatabaseProvider>(TYPES_BOTSHARED.DiscordSlugDatabaseProvider).to(DiscordSlugDatabaseProvider);
myContainer.bind<DiscordSlugService>(TYPES_BOTSHARED.DiscordSlugService).to(DiscordSlugService);

myContainer.bind<ArkServerDatabaseProvider>(TYPES_ARKMANAGEMENT.ArkServerDatabaseProvider).to(ArkServerDatabaseProvider).inSingletonScope();
myContainer.bind<RconService>(TYPES_ARKMANAGEMENT.RconService).to(RconService);
myContainer.bind<ServerQueryService>(TYPES_ARKMANAGEMENT.ServerQueryService).to(ServerQueryService);
myContainer.bind<DataUpdateService>(TYPES_ARKMANAGEMENT.DataUpdateService).to(DataUpdateService);
myContainer.bind<ArkServerManagementConfigDatabaseProvider>(TYPES_ARKMANAGEMENT.ArkServerManagementConfigDatabaseProvider).to(ArkServerManagementConfigDatabaseProvider);
myContainer.bind<ArkManagerIntegrationConfigDatabaseProvider>(TYPES_ARKMANAGERINTEGRATION.ArkManagerIntegrationConfigDatabaseProvider).to(ArkManagerIntegrationConfigDatabaseProvider);

myContainer.bind<ArkManagerAPIService>(TYPES_ARKMANAGERINTEGRATION.ArkManagerAPIService).to(ArkManagerAPIService);

// bot modules
myContainer.bind<BotModule>(TYPES_BOTRUNNER.BotModule).to(DefaultModule).inSingletonScope();
myContainer.bind<ArkManagementBotModule>(TYPES_BOTRUNNER.BotModule).to(ArkManagementBotModule).inSingletonScope();
myContainer.bind<ArkManagerIntegrationBotModule>(TYPES_BOTRUNNER.BotModule).to(ArkManagerIntegrationBotModule).inSingletonScope();

export { myContainer };